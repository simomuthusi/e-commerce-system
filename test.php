$('#button_message').on('click', function() {
  $.ajax({
    url: 'index.php?route=product/add/message',
    type: 'post',
    data: $('form_message').serialize(),
    dataType: 'json',
    beforeSend: function() {
      $('#button-cart').button('loading');
    },
    complete: function() {
      $('#button-cart').button('reset');
    },
    success: function(json) {
      if(json['info']){
       
      if(json['info']['buyer_message'])
      {
        $("#spn_message").html('<div class="alert alert-warning">'+json['info']['buyer_message']+'<button data-dismiss="alert" class="close" type="button">×</button></div>');
      }
      if(json['info']['buyer_email']){
        $("#spn_email").html('<div class="alert alert-warning">'+json['info']['buyer_email']+'<button data-dismiss="alert" class="close" type="button">×</button></div>');
      }
      if(json['success']['success']){
        $("#spn_general").html('<div class="alert alert-success">'+json['success']['success']+'<button data-dismiss="alert" class="close" type="button">×</button></div>');
      }
      }
      else
      {

      }
    }
    });
});