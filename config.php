<?php
// HTTP
define('HTTP_SERVER', 'http://127.0.0.1/open/');

// HTTPS
define('HTTPS_SERVER', 'http://127.0.0.1/open/');

// DIR
define('DIR_APPLICATION', '/var/www/html/open/catalog/');
define('DIR_SYSTEM', '/var/www/html/open/system/');
define('DIR_LANGUAGE', '/var/www/html/open/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/open/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/open/system/config/');
define('DIR_IMAGE', '/var/www/html/open/image/');
define('DIR_CACHE', '/var/www/html/open/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/open/system/download/');
define('DIR_UPLOAD', '/var/www/html/open/system/upload/');
define('DIR_MODIFICATION', '/var/www/html/open/system/modification/');
define('DIR_LOGS', '/var/www/html/open/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dots');
define('DB_DATABASE', 'open');
define('DB_PREFIX', 'open_');
