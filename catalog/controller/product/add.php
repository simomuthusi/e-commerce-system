<?php
DEFINE("OPEN","/open");
class ControllerProductAdd extends Controller {
	private $error = array();

	public function index() {

		// validate fields
		// validate
		if(isset($this->session->data['v_name']))
		{
			$data['v_name']=$this->session->data['v_name']; unset($this->session->data['v_name']);

		}
		else
		{
			$data['v_name']="";
		}
		if(isset($this->session->data['v_description']))
		{
			$data['v_description']=$this->session->data['v_description']; unset($this->session->data['v_description']);

		}
		else
		{
			$data['v_description']="";
		}
		if(isset($this->session->data['v_meta_title']))
		{
			$data['v_meta_title']=$this->session->data['v_meta_title']; unset($this->session->data['v_meta_title']);

		}
		else
		{
			$data['v_meta_title']="";
		}
		if(isset($this->session->data['v_image']))
		{
			$data['v_image']=$this->session->data['v_image']; unset($this->session->data['v_image']);

		}
		else
		{
			$data['v_image']="";
		}
		if(isset($this->session->data['v_model']))
		{
			$data['v_model']=$this->session->data['v_model']; unset($this->session->data['v_model']);

		}
		else
		{
			$data['v_model']="";
		}
		if(isset($this->session->data['v_price']))
		{
			$data['v_price']=$this->session->data['v_price']; unset($this->session->data['v_price']);

		}
		else
		{
			$data['v_price']="";
		}
		if(isset($this->session->data['v_quantity']))
		{
			$data['v_quantity']=$this->session->data['v_quantity']; unset($this->session->data['v_quantity']);

		}
		else
		{
			$data['v_quantity']="";
		}
		if(isset($this->session->data['v_category']))
		{
			$data['v_category']=$this->session->data['v_category']; unset($this->session->data['v_category']);

		}
		else
		{
			$data['v_category']="";
		}
		if(isset($this->session->data['v_seller_first_name']))
		{
			$data['v_seller_first_name']=$this->session->data['v_seller_first_name']; unset($this->session->data['v_seller_first_name']);

		}
		else
		{
			$data['v_seller_first_name']="";
		}
		if(isset($this->session->data['v_seller_name']))
		{
			$data['v_seller_name']=$this->session->data['v_seller_name']; unset($this->session->data['v_seller_name']);

		}
		else
		{
			$data['v_seller_name']="";
		}
		if(isset($this->session->data['v_seller_email']))
		{
			$data['v_seller_email']=$this->session->data['v_seller_email']; unset($this->session->data['v_seller_email']);

		}
		else
		{
			$data['v_seller_email']="";
		}
		if(isset($this->session->data['v_seller_tel']))
		{
			$data['v_seller_tel']=$this->session->data['v_seller_tel']; unset($this->session->data['v_seller_tel']);

		}
		else
		{
			$data['v_seller_tel']="";
		}

		

		if($this->customer->isLogged()) {
			$data['seller_first_name']=$this->customer->getFirstName();
			$data['seller_last_name']=$this->customer->getLastName();
			$data['seller_email']=$this->customer->getEmail();
			$data['seller_tel']=$this->customer->getTelephone();
		}
		else
		{
			$data['seller_first_name']="";
			$data['seller_last_name']="";
			$data['seller_email']="";
			$data['seller_tel']="";
		}

		// add errors
		if(isset($this->session->data['product_save']))
		{
			$data['error_product']=$this->session->data['product_save']; unset($this->session->data['product_save']);
		}
		else
		{
			$data['error_product']="";
		}



		$this->load->language('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		// Countries
		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();


		// Categories
		$this->load->model('catalog/category');

		if (isset($this->request->post['product_category'])) {
			$categories = $this->request->post['product_category'];
		} elseif (isset($this->request->get['product_id'])) {
			$categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
		} else {
			$categories = array();
		}

		$data['product_categories'] = array();

		foreach ($categories as $category_id) {
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$data['product_categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
				);
			}
		}

		// Images
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($product_info) && is_file(DIR_IMAGE . $product_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['product_image'])) {
			$product_images = $this->request->post['product_image'];
		} elseif (isset($this->request->get['product_id'])) {
			$product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
		} else {
			$product_images = array();
		}

		$data['product_images'] = array();

		foreach ($product_images as $product_image) {
			if (is_file(DIR_IMAGE . $product_image['image'])) {
				$image = $product_image['image'];
				$thumb = $product_image['image'];
			} else {
				$image = '';
				$thumb = 'no_image.png';
			}

			$data['product_images'][] = array(
				'image'      => $image,
				'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
				'sort_order' => $product_image['sort_order']
			);
		}

		// Breadcrumbs

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/home', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_ean'] = $this->language->get('entry_ean');
		$data['entry_jan'] = $this->language->get('entry_jan');
		$data['entry_isbn'] = $this->language->get('entry_isbn');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_shipping'] = $this->language->get('entry_shipping');
		$data['entry_date_available'] = $this->language->get('entry_date_available');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length_class'] = $this->language->get('entry_length_class');
		$data['entry_length'] = $this->language->get('entry_length');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_recurring'] = $this->language->get('entry_recurring');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_sku'] = $this->language->get('help_sku');
		$data['help_upc'] = $this->language->get('help_upc');
		$data['help_ean'] = $this->language->get('help_ean');
		$data['help_jan'] = $this->language->get('help_jan');
		$data['help_isbn'] = $this->language->get('help_isbn');
		$data['help_mpn'] = $this->language->get('help_mpn');
		$data['help_minimum'] = $this->language->get('help_minimum');
		$data['help_manufacturer'] = $this->language->get('help_manufacturer');
		$data['help_stock_status'] = $this->language->get('help_stock_status');
		$data['help_points'] = $this->language->get('help_points');
		$data['help_category'] = $this->language->get('help_category');
		$data['help_filter'] = $this->language->get('help_filter');
		$data['help_download'] = $this->language->get('help_download');
		$data['help_related'] = $this->language->get('help_related');
		$data['help_tag'] = $this->language->get('help_tag');

		$data['button_save'] = "Save Product";//$this->language->get('button_save');
		$data['cancel'] = "Cancel";
		$data['action'] = OPEN."/index.php?route=product/add/save";
		$data['button_cancel'] = "Cancel";//$this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
		$data['button_option_add'] = $this->language->get('button_option_add');
		$data['button_option_value_add'] = $this->language->get('button_option_value_add');
		$data['button_discount_add'] = $this->language->get('button_discount_add');
		$data['button_special_add'] = $this->language->get('button_special_add');
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_recurring_add'] = $this->language->get('button_recurring_add');

		$data['entry_country'] = "Select country";//$this->language->get('entry_country');
		$data['entry_zone'] = "Select zone";//$this->language->get('entry_zone');

		$data['tab_general'] = "General infomation";//$this->language->get('tab_general');
		$data['tab_data'] = "Data";//$this->language->get('tab_data');
		$data['tab_attribute'] = "Attributes";//$this->language->get('tab_attribute');
		$data['tab_option'] = "Options";//$this->language->get('tab_option');
		$data['tab_recurring'] = "Recurring";//$this->language->get('tab_recurring');
		$data['tab_discount'] = "Discount";//$this->language->get('tab_discount');
		$data['tab_special'] = "Special";//$this->language->get('tab_special');
		$data['tab_image'] = "Images";//$this->language->get('tab_image');
		$data['tab_links'] = "Links";//$this->language->get('tab_links');
		$data['tab_reward'] = "Reward";//$this->language->get('tab_reward');
		$data['tab_design'] = "Design";//$this->language->get('tab_design');
		$data['tab_openbay'] = "Openbay";//$this->language->get('tab_openbay');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['model'])) {
			$data['error_model'] = $this->error['model'];
		} else {
			$data['error_model'] = '';
		}
		if (isset($this->error['country'])) {
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$data['error_zone'] = $this->error['zone'];
		} else {
			$data['error_zone'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product_form.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/product_form.tpl', $data));
			}
		}

		// save product
		public function save()
		{
			$json=true;
			// $this->session->data['product_save']="Posted successfully";
			
			if(empty($this->request->post['product_description'][1]['name']))
			{
				$this->session->data['v_name']="name required"; $json=false;
			}
			if(empty($this->request->post['product_description'][1]['description']))
			{
				$this->session->data['v_description']="description required"; $json=false;
			}
			if(empty($this->request->post['product_description'][1]['meta_title']))
			{
				$this->session->data['v_meta_title']="Tag title required"; $json=false;
			}
			if(empty($this->request->post['image']))
			{
				$this->session->data['v_image']="Image required! Upload the product image"; $json=false;
			}
			if(empty($this->request->post['model']))
			{
				$this->session->data['v_model']="product model required"; $json=false;
			}
			if(empty($this->request->post['price']))
			{
				$this->session->data['v_price']="price required"; $json=false;
			}
			if(empty($this->request->post['quantity']))
			{
				$this->session->data['v_quantity']="Quantity required"; $json=false;
			}
			if(empty($this->request->post['category']))
			{
				$this->session->data['v_category']="Product category required"; $json=false;
			}
			if(empty($this->request->post['seller_first_name']))
			{
				$this->session->data['v_seller_first_name']="First name required"; $json=false;
			}
			if(empty($this->request->post['v_seller_name']))
			{
				$this->session->data['v_seller_name']="Last name required"; $json=false;
			}
			if(empty($this->request->post['seller_email']))
			{
				$this->session->data['v_seller_email']="Email required"; $json=false;
			}
			if(empty($this->request->post['seller_tel']))
			{
				$this->session->data['v_seller_tel']="Phone number required"; $json=false;
			}

			if($json)
			{
			$this->load->model('catalog/saveproduct');
			if($this->model_catalog_saveproduct->addProduct($this->request->post))
			{
				$this->session->data['product_save']="Posted successfully";
			}
			else
			{
				$this->session->data['product_save']="Error in posting your product";
			}
			}
			$this->response->redirect($this->url->link('product/add', '', 'SSL'));

		}

		// save product
		public function message()
		{
			// var_dump($this->request->post); exit();
			$json=array();
			if(empty($this->request->post['f_email']))
			{
				$json['info']['f_email']="Error in sending message";
			}
			if(empty($this->request->post['buyer_message']))
			{
				$json['info']['buyer_message']="Enter message";
			}
			if(empty($this->request->post['buyer_email']))
			{
				$json['info']['buyer_email']="email address missing";
			}

			if($json)
			{
				echo json_encode($json);
			}
			else
			{
				$subject="User request for product". $post['f_product'];
				$message="<p>This user is requesting for this product : ". $post['f_product']."</p>
				<p>Message : ".$post['buyer_message']."</p>
				<p>Buyer email : ".$post['buyer_email']."</p>"
				;

				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($post['buyer_email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
				// Send user email address
				$json['success']['success']="Information send to seller";
				echo json_encode($json);
			}
		}

}
